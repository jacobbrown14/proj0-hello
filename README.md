README for proj0-hello

Author: Jacob Brown
Email: jbrown14@uoregon.edu

Program Description: "hello.py" pulls message "Hello world" from "credentials.ini" and prints it.

For: CIS 322, Fall 2018, University of Oregon
